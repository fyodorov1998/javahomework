package com.java;

/*На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
        Считать эти данные в массив объектов.
        Вывести в отсортированном по возрастанию веса порядке.*/

public class Human implements Comparable<Human>{
    private double weight;
    private String name;

    public Human(double weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight < 0) {
            weight = 0;
        }
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Имя=" + name + ", Вес='" + weight + '\'';
    }

    //реализуем интерфейс для сравнения по весу
    @Override
    public int compareTo(Human o) {
        return (int) (this.weight - o.getWeight());
    }
}
