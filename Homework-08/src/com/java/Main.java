package com.java;

/*На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
        Считать эти данные в массив объектов.
        Вывести в отсортированном по возрастанию веса порядке.*/

import java.util.*;


public class Main {

    //перечисление преобразуем в список имен
    static final List<Names> NAMES =
            Collections.unmodifiableList(Arrays.asList(Names.values()));
    static Random random = new Random();

    public static String randomName(List <Names> NAMES)  {
        return NAMES.get(random.nextInt(NAMES.size())).toString();
    }

    public static void main(String[] args) {
        //генерация 10 человек из списка имен и весом 50-150 кг.
        int n = 10;
        ArrayList <Human> humanArrayList = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            humanArrayList.add(new Human(random.nextInt(150 - 50) + 50, randomName(NAMES)));
        }

        System.out.println("\nИсходный список:");
        for (Human human: humanArrayList){
            System.out.println(human);
        }

        Collections.sort(humanArrayList);

        System.out.println("\nОтсортированный по весу список:");
        for (Human human: humanArrayList){
            System.out.println(human);
        }
    }

}